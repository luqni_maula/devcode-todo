import React, {memo, Suspense, lazy} from "react";
import {Route, Switch} from "react-router-dom";
import FallbackContent from "@app/components/FallbackContent";

export default memo(({match}) => (
	<Suspense fallback={<FallbackContent/>}>
	    <Switch>
	        <Route exact path={`${match.url}`} component={lazy(() => import('./ToDo'))}/>
	        <Route path={`${match.url}detail`} component={lazy(() => import('./Detail'))}/>
	    </Switch>
	</Suspense>
));