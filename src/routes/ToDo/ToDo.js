import React, {memo, /*useEffect,*/ useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {createTodo/*, getTodo*/} from "@reduxActions/Todo";
import ActivityItem from "@app/components/Activity/Item";

export default memo(() => 
{
	const dispatch = useDispatch();
	const {loading, data} = useSelector(({todo}) => todo);

	// useEffect(() => {
	// 	dispatch(getTodo());
	// }, [dispatch]);

	const [adding, setAdding] = useState(false);
	const onCreateTodo = () =>
	{
		setAdding(true);
		dispatch(createTodo(() => setAdding(false)));
	}

	return (
		<div className="todo-w-100">
			<div className="todo-d-flex todo-justify-content-between todo-my-4 todo-align-items-center">
				<h1 className="todo-my-0 todo-font-weight-semi-bold"
				data-cy="activity-title">
					Activity
				</h1>
				<div>
					<button
					data-cy="activity-add-button"
					className="todo-btn todo-btn-primary"
					onClick={onCreateTodo}
					disabled={adding}>
						{adding ? <span>Menambahkan</span> : (
							<React.Fragment>
								<span className="todo-icon-plus todo-mr-2"/>
								Tambah
							</React.Fragment>
						)}
					</button>
				</div>
			</div>
			{loading && (
				<div className="todo-w-100 todo-text-center">
					<span>Loading activity...</span>
				</div>
			)}
			<div className="todo-activities">
				{data.map((row, index) => (
			        <div key={index} className="todo-p-2">
			        	<ActivityItem data={row}/>
				    </div>
				))}
			</div>
			{!(data && data.length) && (
				<div className="todo-w-100 todo-text-center todo-mb-5">
					<span className="todo-activity-empty"
					data-cy="activity-empty-state"/>
				</div>
			)}
		</div>
	)
});