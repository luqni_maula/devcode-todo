import React, {memo, useState, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {useParams, useHistory} from "react-router-dom";
import axios from "@util/Api";
import RouteAccess from "@config/RouteAccess";
import {setError} from "@reduxActions/Common";
import {getTodoItems} from "@reduxActions/Todo";
import StateItem from "@app/components/Activity/State/Item";
import AddState from "@app/components/Activity/State/Add";
import Sorters from "@app/components/Activity/State/Sorter";

export default memo(() => 
{
	const dispatch = useDispatch();
	const history = useHistory();
	const {slug} = useParams();

	const [editable, setEditable] = useState(false);
	useEffect(() => {
		if (editable) document.getElementById('edit-activity-title').focus();
	}, [editable]);

	const [record, setRecord] = useState(null);
	const [actTitle, setActTitle] = useState('');
	useEffect(() => {
		(async () => {
			try {
				const request = await axios.get(RouteAccess.getApi('TODO_ACTIVITY_ID', {id: slug}));
				const {data} = request ?? {};
				setRecord(data);
			} catch(error) {
				dispatch(setError(error));
			}
		})();

		dispatch(getTodoItems(slug));
	}, [slug, dispatch]);

	const {title} = record ?? {};

	useEffect(() => setActTitle(title), [title]);

	const onChangeTitle = async () =>
	{
		setEditable(!editable);
		try {
			await axios.patch(RouteAccess.getApi('TODO_ACTIVITY_ID', {id: slug}), {title: actTitle});
		} catch(error) {
			dispatch(setError(error));
		}
	}

	const {item_loading, item_data} = useSelector(({todo}) => todo);

	return (
		<div className="todo-w-100">
			<div className="todo-d-flex todo-justify-content-between todo-my-4 todo-align-items-center">
				<div className="todo-d-inline-flex todo-justify-content-between todo-align-items-center">
					<span
					className="todo-icon-back todo-mr-2 todo-pointer"
					data-cy="todo-back-button"
					onClick={() => history.push('/')}/>
					{editable ? (
						<input
						id="edit-activity-title"
						type="text"
						value={actTitle}
						onChange={({target}) => setActTitle(target.value)}
						className="todo-activity-edit todo-font-weight-semi-bold"
						data-cy="todo-title"
						onBlur={onChangeTitle}/>
					) : (
						<h1 className="todo-my-0 todo-font-weight-semi-bold"
						data-cy="todo-title"
						onClick={() => setEditable(!editable)}>
							{actTitle}
						</h1>
					)}
					<span className="todo-icon-edit todo-ml-3 todo-pointer"
					data-cy="todo-title-edit-button"
					onClick={() => setEditable(!editable)}/>
				</div>
				<div>
					<Sorters/>
					<AddState/>
				</div>
			</div>
			{item_loading && (
				<div className="todo-w-100 todo-text-center">
					<span>Loading items...</span>
				</div>
			)}
			{item_data.map((row, index) => (
				<div key={index} className="todo-mb-2 todo-w-100">
		        	<StateItem data={row}/>
			    </div>
			))}
			{!(item_data && item_data.length) && (
				<div className="todo-w-100 todo-text-center todo-mb-5">
					<span className="todo-state-empty" data-cy="todo-empty-state"/>
				</div>
			)}
		</div>
	)
});