// import axios from 'axios';

// axios.create({
//     headers: {
//         'Cache-Control': 'no-cache',
//         'Content-Type': 'application/json',
//     },
//     withCredentials: true,
// });

// axios.interceptors.request.use((config) => {
//     return config;
// });

// axios.interceptors.response.use((response) => {
// 	return response;
// }, (error) => {
// 	return Promise.reject(error);
// });

// export default axios;

const toQueryString = (object) =>
{
  	let str = [];
  	for (let key in object)
  	{
    	if (object.hasOwnProperty(key)) str.push(key + "=" + object[key]);
    }
    let val = `?${str.join("&")}`;
  	if (val.trim() === '?') return '';
  	return val;
}

export default {
	get: (url, options) =>
	{
		return new Promise((resolve, reject) => {
			let qs = toQueryString(options?.params ?? {});
			window.fetch(url + qs, {
				method: 'GET',
				cache: 'no-cache'
			}).then(async (res) => res.json())
			.then((data) => {
				resolve({data});
			}).catch((e) => {
				reject(e);
			});
		});
	},
	post: (url, payload) =>
	{
		return new Promise((resolve, reject) => {
			window.fetch(url, {
				method: 'POST',
				cache: 'no-cache',
				body: new URLSearchParams(payload),
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).then(async (res) => res.json())
			.then((data) => {
				resolve({data});
			}).catch((e) => {
				reject(e);
			});
		});
	},
	patch: (url, payload) =>
	{
		return new Promise((resolve, reject) => {
			window.fetch(url, {
				method: 'PATCH',
				cache: 'no-cache',
				body: new URLSearchParams(payload),
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).then(async (res) => res.json())
			.then((data) => {
				resolve({data});
			}).catch((e) => {
				reject(e);
			});
		});
	},
	delete: (url) =>
	{
		return new Promise((resolve, reject) => {
			window.fetch(url, {
				method: 'DELETE',
				cache: 'no-cache'
			}).then(async (res) => res.json())
			.then((data) => {
				resolve({data});
			}).catch((e) => {
				reject(e);
			});
		});
	}
};