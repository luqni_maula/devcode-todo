export const priorities = [
	{
		title: 'Very High',
		value: 'very-high',
		styleName: 'todo-label-indicator todo-very-high',
	},
	{
		title: 'High',
		value: 'high',
		styleName: 'todo-label-indicator todo-high',
	},
	{
		title: 'Normal',
		value: 'normal',
		styleName: 'todo-label-indicator todo-normal',
	},
	{
		title: 'Low',
		value: 'low',
		styleName: 'todo-label-indicator todo-low',
	},
	{
		title: 'Very Low',
		value: 'very-low',
		styleName: 'todo-label-indicator todo-very-low',
	},
];