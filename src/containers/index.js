import React, {memo, useEffect} from "react";
import {useHistory} from "react-router-dom";
import {useDispatch} from "react-redux";
import InfoView from "@app/components/InfoView";
import Routes from "@routes";
import {getTodo} from "@reduxActions/Todo";

const AppHeader = memo(() => (
	<div className="todo-header" data-cy="header-background">
		<div className="todo-container">
			<h2 data-cy="header-title"
			className="todo-wrapper-container todo-header-title">
				TO DO LIST APP
			</h2>
		</div>
	</div>
));

export default memo(({match}) =>
{
	const dispatch = useDispatch();
	const {pathname} = useHistory()?.location;

	useEffect(() => {
		if (pathname === '/') dispatch(getTodo());
	}, [dispatch, pathname]);

	return (
		<div className="todo-w-100">
			<AppHeader/>
			<div className="todo-wrapper-container">
				<Routes match={match}/>
			</div>
			<InfoView/>
        </div>
	)
});