export default {
	MIDDLEWARE: process.env.REACT_APP_MIDDLEWARE === 'true',
	BASE_API: process.env.REACT_APP_BASE_API,
};