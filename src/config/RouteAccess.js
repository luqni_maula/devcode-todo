import Config from '@config/Config';

const {BASE_API} = Config;

const RouteAccess = {
    getApi: function(name, object) {
        if (this.hasOwnProperty(name)) {
        	let url = this[name];
        	for (let key in (object ?? {})) {
        		url = url.replace(`{{${key}}}`, object[key]);
        	}
        	return url;
        }
        return null;
    },
    TODO_ACTIVITY: BASE_API + '/activity-groups',
    TODO_ACTIVITY_ID: BASE_API + '/activity-groups/{{id}}',
    TODO_ACTIVITY_ITEM: BASE_API + '/todo-items',
    TODO_ACTIVITY_ITEM_ID: BASE_API + '/todo-items/{{id}}',
};

export default RouteAccess;
