import React, {memo, useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux"
import {hideMessage} from "@reduxActions/Common";

export default memo(() =>
{
    const dispatch = useDispatch();
    const errorMessage = useSelector(({common}) => common.error);
    const infoMessage = useSelector(({common}) => common.message);

    const [showInfo, setShowInfo] = useState(false);
    useEffect(() => {
        if (infoMessage) setShowInfo(true);
    }, [infoMessage]);

    const [showError, setShowError] = useState(false);
    useEffect(() => {
        if (errorMessage) setShowError(true);
    }, [errorMessage]);

    useEffect(() => 
    {
        // handle modal close outside of modal content
        const handleModalVisibility = (e) => {
            const modalParent = document.querySelector('.todo-modal-info');
            if (modalParent && !modalParent.contains(e.target)) 
            {
                dispatch(hideMessage());
                setShowInfo(false);
            }
        };

        if (showInfo) {
            document.body.addEventListener('click', handleModalVisibility);
        } else {
            document.body.removeEventListener('click', handleModalVisibility);
        }

        return () => document.body.removeEventListener('click', handleModalVisibility);
    }, [showInfo, dispatch]);

    useEffect(() => 
    {
        // handle modal close outside of modal content
        const handleModalVisibility = (e) => {
            const modalParent = document.querySelector('.todo-modal-error');
            if (modalParent && !modalParent.contains(e.target)) 
            {
                dispatch(hideMessage());
                setShowError(false);
            }
        };

        if (showError) {
            document.body.addEventListener('click', handleModalVisibility);
        } else {
            document.body.removeEventListener('click', handleModalVisibility);
        }

        return () => document.body.removeEventListener('click', handleModalVisibility);
    }, [showError, dispatch]);

    return (
        <React.Fragment>
            {showInfo && (
                <div className="todo-modal-wrapper">
                    <div className="todo-modal-content todo-modal-info" data-cy="modal-information">
                        <div className="todo-d-inline-flex todo-justify-content-between todo-align-items-center todo-my-4">
                            <span className="todo-icon-info-circle todo-text-primary todo-mr-2 todo-fs-lg" data-cy="modal-information-icon"/>
                            <span data-cy="modal-information-title">{infoMessage}</span>
                        </div>
                    </div>
                </div>
            )}
            {showError && (
                <div className="todo-modal-wrapper">
                    <div className="todo-modal-content todo-modal-error" data-cy="modal-information">
                        <div className="todo-d-inline-flex todo-justify-content-between todo-align-items-center todo-my-4">
                            <span className="todo-icon-info-circle todo-text-danger todo-mr-2 todo-fs-lg" data-cy="modal-information-icon"/>
                            <span data-cy="modal-information-title">{errorMessage}</span>
                        </div>
                    </div>
                </div>
            )}
        </React.Fragment>
    );
});