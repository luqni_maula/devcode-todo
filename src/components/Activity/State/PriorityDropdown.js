import React, {memo, useState, useEffect} from "react";
import {priorities} from "@constants/General";

export default memo(({value, onChange, cyMode}) =>
{
	const [expanded, setExpanded] = useState(false);
	const handleExpand = () => setExpanded((value) => !value);

	const selectedData = priorities.find(({value: val}) => value === val);
	const {
		title,
		styleName
	} = selectedData ?? {};

	useEffect(() => {
		if (!value) onChange(null, priorities[0]?.value ?? '');
	}, [value, onChange]);

	return (
    	<React.Fragment>
	    	<div className="todo-priority-box" 
			data-cy={`modal-${cyMode}-priority-dropdown`}
			onClick={handleExpand}>
	    		<div className="todo-d-flex todo-justify-content-between todo-p-2">
		            <div className="todo-d-flex todo-justify-content-between todo-align-items-center">
	    				<div className={styleName}/>{title}
		            </div>
	    			<div className="todo-icon-chevron-down"/>
	    		</div>
		    	{expanded && (
		    		<div className="todo-priority-dropdown todo-w-100">
			    		<div className="todo-d-flex todo-justify-content-between todo-priority-dropdown-item todo-p-2 todo-bg-light-grey">
			    			<h4 className="todo-font-weight-semi-bold">Pilih priority</h4>
			    			<div className="todo-icon-chevron-up"/>
			    		</div>
                		{priorities.map(({title, value: val, styleName}, index) => (
				    		<div key={index}
				    		className="todo-d-flex todo-justify-content-between todo-priority-dropdown-item todo-p-2"
				    		data-cy={`modal-${cyMode}-priority-item`}
				    		onClick={(e) => {
				    			e.stopPropagation();
				    			setExpanded(false);
				    			onChange(e, val);
				    		}}>
		              			<div className="todo-d-flex todo-justify-content-between todo-align-items-center">
			              			<div className={styleName}/>{title}
			              			{value === val && <div className="todo-icon-check todo-ml-4"/>}
		              			</div>
				    		</div>
                		))}
		    		</div>
		    	)}
	    	</div>
    	</React.Fragment>
	)
});