import React, {memo, useState, useEffect} from "react";
import {useParams} from "react-router-dom";
import {useDispatch} from "react-redux";
import axios from "@util/Api";
import RouteAccess from "@config/RouteAccess";
import {getTodoItems} from "@reduxActions/Todo";
import {setError, fetchError} from "@reduxActions/Common";
import PriorityDropdown from "@app/components/Activity/State/PriorityDropdown";

export default memo(({data}) => 
{
	const {id, title, priority} = data;

	const {slug} = useParams();
	const dispatch = useDispatch();

	const [activityName, setActivityName] = useState('');
	const [activityPriority, setActivityPriority] = useState('');

	const [loading, setLoading] = useState(false);
	const [allowSubmit, setAllowSubmit] = useState(false);

	const [show, setShow] = useState(false);
	const handleShow = () => {
		if (!loading) setShow((value) => !value);
	};

	const onEditItem = (e) =>
	{
		if (e) e.preventDefault();
		if (activityName && activityPriority && allowSubmit)
		{
			(async () => {
				const payload = {
					title: activityName ?? '',
					priority: activityPriority ?? ''
				};

				try {
					setLoading(true);
					await axios.patch(RouteAccess.getApi('TODO_ACTIVITY_ITEM_ID', {id}), payload);
					handleShow();
					dispatch(getTodoItems(slug));
				} catch(error) {
					dispatch(setError(error));
				} finally {
					setLoading(false);
				}
			})();
		} else {
			dispatch(fetchError('Mohon lengkapi form'));
		}
	}

    useEffect(() =>
    {
    	if (activityName && activityPriority) {
    		setAllowSubmit(true);
    	} else {
    		setAllowSubmit(false);
    	}
    }, [activityName, activityPriority]);

	useEffect(() =>
	{
		if (show)
		{
			setActivityName(title);
			setActivityPriority(priority);
		}
	}, [show, title, priority]);

    useEffect(() =>
    {
    	if (activityName && activityPriority) {
    		setAllowSubmit(true);
    	} else {
    		setAllowSubmit(false);
    	}
    }, [activityName, activityPriority]);

    useEffect(() => 
	{
		// handle modal close outside of modal content
		const handleModalVisibility = (e) => {
			const modalParent = document.querySelector('.todo-modal-content');
			if (modalParent && !modalParent.contains(e.target)) setShow(false);
		};

		if (show) {
			document.body.addEventListener('click', handleModalVisibility);
		} else {
			document.body.removeEventListener('click', handleModalVisibility);
		}
	}, [show]);

	return (
		<React.Fragment>
			<span className="todo-icon-state-item todo-ml-3 todo-pointer"
			data-cy="todo-item-edit-button"
			onClick={handleShow}/>
			{show && (
				<div className="todo-modal-wrapper">
					<div className="todo-modal-content" data-cy="modal-edit">
						<div className="todo-w-100">
							<div className="todo-d-flex todo-justify-content-between todo-p-4">
								<h3 className="todo-font-weight-semi-bold todo-mb-0" data-cy="modal-edit-title">Edit Item</h3>
								<div className="todo-font-weight-semi-bold todo-pointer todo-icon-close" onClick={handleShow}/>
							</div>
			                <div className="todo-divider todo-mb-3"/>
			                <form onSubmit={onEditItem} className="todo-w-100">
			                	<div className="todo-w-100 todo-px-4">
				                	<div className="todo-w-100 todo-mb-2 todo-text-left">
				                		<h3 data-cy="modal-edit-name-title">
				                			<b><span className="todo-text-danger todo-mr-2">*</span>NAMA LIST ITEM</b>
				                		</h3>
				                	</div>
				                	<input
				                	type="text"
				                	className="todo-text-input"
				                	value={activityName}
				                	data-cy="modal-edit-name-input"
				                	onChange={({target}) => setActivityName(target.value)}/>
			                	</div>
			                	<div className="todo-w-100 todo-px-4">
				                	<div className="todo-w-100 todo-mb-2 todo-text-left todo-mt-4">
				                		<h3 data-cy="modal-edit-priority-title">
				                			<b><span className="todo-text-danger todo-mr-2">*</span>PRIORITY</b>
				                		</h3>
				                	</div>
				                	<PriorityDropdown
				                	cyMode="edit"
				                	value={activityPriority}
				                	onChange={(e, value) => setActivityPriority(value)}/>
			                	</div>
			                	<div className="todo-divider todo-mt-4 todo-mb-1"/>
			                	<div className="todo-text-right todo-mx-4 todo-my-3">
			                    	<button
									className="todo-btn todo-btn-primary"
									disabled={loading || !allowSubmit}
									type="submit"
									data-cy="modal-edit-save-button">
										{loading ? <span>Menyimpan</span> : 'Simpan'}
									</button>
			                    </div>
			                </form>
						</div>
					</div>
				</div>
			)}
		</React.Fragment>
	)
});