import React, {memo, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {sortItems} from "@reduxActions/Todo";

const sorterList = [
	{
		title: 'Terbaru',
		value: 'newest',
		icon: 'todo-icon-sort-desc',
	},
	{
		title: 'Terlama',
		value: 'oldest',
		icon: 'todo-icon-sort-asc',
	},
	{
		title: 'A-Z',
		value: 'az',
		icon: 'todo-icon-sort-asc-alp',
	},
	{
		title: 'Z-A',
		value: 'za',
		icon: 'todo-icon-sort-desc-alp',	
	},
	{
		title: 'Belum Selesai',
		value: 'unifinished',
		icon: 'todo-icon-sort-unfinished',
	},
];

export default memo(() =>
{
	const dispatch = useDispatch();
	const {item_sorter} = useSelector(({todo}) => todo);

	const [showSorter, setShowSorter] = useState(false);
	const handleShowSorter = () => setShowSorter((v) => !v);

	return (
		<React.Fragment>
			<button className="todo-btn-sort todo-pointer"
			data-cy="todo-sort-button"
			onClick={handleShowSorter}>
				<span className="todo-icon-sort"/>
			</button>
			{showSorter && (
				<div className="todo-sorter-parent" data-cy="sort-parent">
		  			{sorterList.map(({title, icon, value}) => (
						<div className="todo-sorter-item todo-p-2 todo-pointer"
						data-cy="sort-selection"
			    		onClick={() => {
			    			dispatch(sortItems(value));
			    			handleShowSorter();
			    		}}>
							<div className="todo-d-inline-flex todo-justify-content-between todo-align-items-center">
						    	{icon && <span className={`${icon} todo-mr-2`} data-cy="sort-selection-icon"/>}
						    	<span data-cy="sort-selection-title">{title}</span>
							</div>
			    			{item_sorter === value && <div className="todo-icon-check todo-ml-4" data-cy="sort-selection-selected"/>}
						</div>
		  			))}
				</div>
			)}
		</React.Fragment>
	)
});