import React, {memo, useState, useEffect} from "react";
import {useParams} from "react-router-dom";
import {useDispatch} from "react-redux";
import {getTodoItems, deleteItem} from "@reduxActions/Todo";

export default memo(({id, title}) =>
{
	const {slug} = useParams();
	const dispatch = useDispatch();
	const [show, setShow] = useState(false);
	const [deleting, setDeleting] = useState(false);

	const handleShow = () => setShow((show) => !show);
	const onDelete = () =>
	{
		handleShow();
		setDeleting(true);
		dispatch(deleteItem(id, () => {
			setDeleting(false);
			dispatch(getTodoItems(slug));
		}));
	}

	useEffect(() => 
	{
		// handle modal close outside of modal content
		const handleModalVisibility = (e) => {
			const modalParent = document.querySelector('.todo-modal-content');
			if (modalParent && !modalParent.contains(e.target)) setShow(false);
		};

		if (show) {
			document.body.addEventListener('click', handleModalVisibility);
		} else {
			document.body.removeEventListener('click', handleModalVisibility);
		}

		return () => document.body.removeEventListener('click', handleModalVisibility);
	}, [show]);

	return (
		<React.Fragment>
			{deleting ? <span>...</span> : (
				<span className="todo-icon-trash todo-pointer"
				data-cy="todo-item-delete-button"
				onClick={handleShow}/>
			)}
			{show && (
				<div className="todo-modal-wrapper">
					<div className="todo-modal-content" data-cy="modal-delete">
						<div className="todo-w-100 todo-text-center todo-p-4">
							<i className="todo-icon-warning todo-text-danger todo-fs-iconcard"
							data-cy="modal-delete-icon"/>
							<h3 className="todo-my-4"
							data-cy="modal-delete-title">
								<span className="todo-font-weight-normal">Apakah anda yakin ingin menghapus item </span>
								<span className="todo-font-weight-semi-bold">"{title}"</span> ?
							</h3>
							<button
							className="todo-btn todo-mr-2"
							onClick={handleShow}
							data-cy="modal-delete-cancel-button">
								Batal
							</button>
							<button
							className="todo-btn todo-btn-danger"
							onClick={onDelete}
							data-cy="modal-delete-confirm-button">
								Hapus
							</button>
						</div>
					</div>
				</div>
			)}
		</React.Fragment>
	)
});