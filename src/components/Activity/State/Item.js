import React, {memo, useState, useEffect} from "react";
import {useParams} from "react-router-dom";
import {useDispatch} from "react-redux";
import {priorities} from "@constants/General";
import ItemDelete from "@app/components/Activity/State/Delete";
import EditState from "@app/components/Activity/State/Edit";
import {getTodoItems, setActiveItem} from "@reduxActions/Todo";

export default memo(({data}) =>
{
	const {slug} = useParams();
	const dispatch = useDispatch();
	const {id, title, priority, is_active} = data;
	const {styleName: priorityStyleName} = priorities.find(({value}) => value === priority) ?? {};

	const [active, setActive] = useState(is_active);
	useEffect(() => {
		setActive(is_active);
	}, [is_active]);

	const [checking, setChecking] = useState(false);
	const handleCheck = ({target}) =>
	{
		const {checked} = target;
		setChecking(true);
		dispatch(setActiveItem(id, !checked, () => {
			setActive(!checked);
			setChecking(false);
			dispatch(getTodoItems(slug));
		}));
	}

	return (
		<div className="todo-state-item todo-p-4" data-cy="todo-item">
			<div className="todo-d-inline-flex todo-justify-content-between todo-align-items-center">
				{checking ? (
					<div>...</div>
				) : (
					<input
					type="checkbox"
					checked={!active}
					onChange={handleCheck} 
					className="todo-mr-2 todo-pointer"
					data-cy="todo-item-checkbox"/>
				)}
				{priorityStyleName && <div className={priorityStyleName} data-cy="todo-item-priority-indicator"/>}
				<div className="todo-d-inline-flex todo-justify-content-between todo-align-items-center">
					<p className={`todo-my-0 todo-font-weight-semi-bold ${!active && 'todo-text-item-completed'}`}
					data-cy="todo-item-title">
						{title}
						<EditState data={data}/>
					</p>
				</div>
			</div>
			<div style={{float: 'right'}}>
				<ItemDelete {...data}/>
			</div>
		</div>
	)
});