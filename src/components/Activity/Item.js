import React, {memo} from "react";
import {Link} from "react-router-dom";
import ActivityDelete from "@app/components/Activity/Delete";

const getDateFormat = (dateString) =>
{
	const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
	const d = new Date(dateString);
	return `${d.getDate()} ${monthNames[d.getMonth()]} ${d.getFullYear()}`;
}

export default memo(({data}) =>
{
	const {id, title, created_at} = data;
	return (
		<div className="todo-card">
			<Link to={`/detail/${id}`} data-cy="activity-item">
				<div className="todo-card-body">
					<h4 data-cy="activity-item-title"
					className="todo-card-item-title todo-mt-0">
						{title}
					</h4>
				</div>
			</Link>
			<div className="todo-d-flex todo-justify-content-between">
				<div className="todo-text-muted todo-fs-md" data-cy="activity-item-date">
					{created_at ? getDateFormat(created_at) : '-'}
				</div>
				<div className="todo-fs-md">
					<ActivityDelete {...data}/>
				</div>
			</div>
		</div>
	)
});