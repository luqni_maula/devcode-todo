import React, {memo} from "react";

const FallbackContent = () => (
	<div className="todo-w-100 todo-h-100 todo-text-center todo-mt-5" style={{verticalAlign: 'middle'}}>
		<h3>Loading...</h3>
    </div>
);

export default memo(FallbackContent);