- run command `yarn` or `npm install`
- after that, run `yarn start` to start development mode
- this application should be run at `http://localhost:8000`

Demo: https://luqni-todo.web.app \
Result: https://devcode.gethired.id/job/BZI687 \
Figma design: https://www.figma.com/file/42zARqf1zwmqZFE45yY2nc/To-Do-List-(Code-Competition) \
Postman collections: https://www.postman.com/collections/b0a48bb59c1511914c46 \

Luqni Maulana\
email: `maulana.7jr@gmail.com`